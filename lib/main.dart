import 'package:flutter/material.dart';

Column _buildButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(icon, color: color),
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      )
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'PERSONAL  PROFILE',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Text(
                    'I am Sasima Muaedrot, My nickname is Fah, am 22 years. '
                    'Extremely motivated to constantly develop my skills and'
                    'grow professional, And working in this field is a job I dream of doing.',
                    style: TextStyle(color: Colors.black)),
              ],
            ),
          ),
        ],
      ),
    );

    Widget textSection1 = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'S K I L L S',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset(
                      'images/html5.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.contain,
                    ),
                    Image.asset(
                      'images/css.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.contain,
                    ),
                    Image.asset(
                      'images/mysql.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.contain,
                    ),
                    Image.asset(
                      'images/Js.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.contain,
                    ),
                    Image.asset(
                      'images/python.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.contain,
                    )
                  ],
                ),
              )
            ],
          ))
        ],
      ),
    );

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.email, 'Email'),
          _buildButtonColumn(color, Icons.facebook, 'Facebook'),
          _buildButtonColumn(color, Icons.call, 'Call'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'EDUCATION',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text('Burapha University', style: TextStyle(color: Colors.black)),
              Text('Faculty of Informatic',
                  style: TextStyle(color: Colors.black)),
              Text('Computer Science', style: TextStyle(color: Colors.black)),
            ],
          ))
        ],
      ),
    );

    Widget textSection2 = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'WORK  EXPERIENCE',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text('July 2020', style: TextStyle(color: Colors.black)),
              Text(
                  'My experience was learning, I started studying software. Designing the UI screen to visualize the application '
                  'format you want to create first, and to give customers a glimpse of the app they wan',
                  style: TextStyle(color: Colors.black)),
            ],
          ))
        ],
      ),
    );

    Widget textSection3 = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'INTERNSHI POBJECTIVES',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                  'I wanted to apply what I had learned in my work to gain experience in doing it before I got out to work and experienced it',
                  style: TextStyle(color: Colors.black)),
            ],
          ))
        ],
      ),
    );

    return MaterialApp(
        title: 'My Resume',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('My Resume'),
          ),
          body: ListView(children: [
            Image.asset('images/me.jpg'),
            titleSection,
            textSection,
            textSection1,
            textSection2,
            textSection3,
            buttonSection,
            // textSection
          ]),
        ));
  }
}
